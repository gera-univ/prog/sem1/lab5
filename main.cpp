#include <iostream>
#include <cmath>
#include <iomanip>
#include <functional>
#include <vector>

using namespace std;

struct Integrable {
    long double left;
    long double right;
    function<long double(double)> f;

    Integrable(function<long double(double)> f, long double left, long double right) : left(left), right(right), f(std::move(f)) {}
};

class NumericIntegral {
private:
    long double precision;
    int stepCounter;
public:
    explicit NumericIntegral(long double precision) : precision(precision) {
        stepCounter = 0;
    }

    long double getPrecision() const {
        return precision;
    }

    int lastStepCount() { return stepCounter; }

    long double trapezoid(const Integrable &integrand) {
        long double dx = (abs(integrand.right - integrand.left));
        long double result = dx * (integrand.f(integrand.left) + integrand.f(integrand.left + dx)) / 2;
        long double prev = 0;
        stepCounter = 1;
        while (abs(result - prev) > precision) {
            prev = result;
            result = 0;
            dx /= 2;
            stepCounter *= 2;

            long double pos = integrand.left;
            while (pos < integrand.right) {
                result += dx * (integrand.f(pos) + integrand.f(pos + dx)) / 2;
                pos += dx;
            }
        }

        return result;
    }

};

int main(int argc, char **argv) {
    long double precision;
    cout << "Enter precision: " << endl;
    cin >> precision;
    if (cin.fail()) {
        cerr << "Incorrect input!";
        return -1;
    }
    if (precision < 1e-10)
        cerr << "Precision is too low!" << endl;
    if (precision <= 0) {
        cerr << "Incorrect precision value!" << endl;
        return -1;
    }

    NumericIntegral ni(precision * 0.1);

    cout << "p = " << ni.getPrecision() << endl;

    vector<Integrable> funcs = {Integrable([](long double x) { return x * x * x * pow(M_E, M_PI * 2); }, 0, 0.8),
                                Integrable([](long double x) { return 1 / (1 + sqrt(x)); }, 0.0, 1.8),
                                Integrable([](long double x) { return 1 / ((x + 1) * sqrt(x * x + 1)); }, 0.0, 0.3)};

    cout << setprecision(10) << scientific;
    for (const Integrable &f : funcs) {
        cout << ni.trapezoid(f) << " " << ni.lastStepCount() << endl;
    }
    return 0;
}
